﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Transvam.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Transvam.Core.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeDetailPage : MvxContentPage<HomeDetailViewModel>
	{
        public HomeDetailPage()
		{
			InitializeComponent();
		}
	}
}
