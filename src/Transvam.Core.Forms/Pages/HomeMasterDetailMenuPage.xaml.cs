﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Transvam.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Transvam.Core.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxMasterDetailPagePresentation(Position = MasterDetailPosition.Master, 
        WrapInNavigationPage = false, NoHistory = true)]
    public partial class HomeMasterDetailMenuPage : MvxContentPage<HomeMasterDetailMenuViewModel>
	{
        public HomeMasterDetailMenuPage()
		{
			InitializeComponent();
		}
	}
}
