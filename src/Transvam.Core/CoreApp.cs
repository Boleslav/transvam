﻿using System;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using Transvam.Core.ViewModels;

namespace Transvam.Core
{
    public class CoreApp : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

//            Mvx.LazyConstructAndRegisterSingleton<IMvxMessenger, MvxMessengerHub>();

            //            RegisterCustomAppStart<CoreAppStart<SecondViewModel>>();
            //            RegisterAppStart<Tab1ViewModel>();
//            RegisterAppStart<HomeMasterDetailMenuViewModel>();
            RegisterAppStart<LoginViewModel>();
        }
    }
}
