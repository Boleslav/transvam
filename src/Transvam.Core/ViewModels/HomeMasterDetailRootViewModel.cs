﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace Transvam.Core.ViewModels
{
    public class HomeMasterDetailRootViewModel : MvxViewModel
    {
        public IMvxCommand GoDetails { get; set; }
        public IMvxCommand GoPopup { get; set; }

        public HomeMasterDetailRootViewModel()
        {
            GoDetails = new MvxCommand(async () => await ProcessLogin());
            GoPopup = new MvxCommand(async () => await ProcessPopup());
        }

        private async Task ProcessLogin()
        {
            await NavigationService.Navigate<HomeDetailViewModel>();
        }
        private async Task ProcessPopup()
        {
            await NavigationService.Navigate<HomePopupViewModel>();
        }
    }
}