﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace Transvam.Core.ViewModels
{
    public class HomePopupViewModel : MvxViewModel
    {
        public IMvxCommand GoCancel { get; set; }
        public IMvxCommand GoPopup { get; set; }

        public HomePopupViewModel()
        {
            GoCancel = new MvxCommand(async () => await ProcessCancel());
        }

        private async Task ProcessCancel()
        {
            await NavigationService.Close(this);
        }

    }
}