﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace Transvam.Core.ViewModels
{
    public class LoginDetailsViewModel : MvxViewModel
    {
        public IMvxCommand GoLogin { get; set; }

        public LoginDetailsViewModel()
        {
            GoLogin = new MvxCommand(async () => await ProcessLogin());
        }

        private async Task ProcessLogin()
        {
            await NavigationService.Navigate<HomeMasterDetailMenuViewModel>();
            await NavigationService.Navigate<HomeMasterDetailRootViewModel>();
        }
    }
}