﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace Transvam.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        public IMvxCommand GoLogin { get; set; }

        public LoginViewModel()
        {
            GoLogin = new MvxCommand(async () => await ProcessLogin());
        }

        private async Task ProcessLogin()
        {
            await NavigationService.Navigate<LoginDetailsViewModel>();
        }
    }
}